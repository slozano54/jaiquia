# J'ai ... Qui a ...

## Description
Propositions de jeux de cartes autour d'un J'ai ... Qui a ...

## Contributing
Les propositions de jeux sont les bienvenues.
## Authors and acknowledgment
Membres du groupe jeux IREM de Lorraine.

## License
CC BY-NC-SA 4.0 Copyright (c) [2022 - IREM DE LORRAINE - GROUPE JEUX](https://framagit.org/slozano54/jaiquia/-/blob/master/LICENCE.md)

## Project status
Transfert sur LaForgeEdu [https://forge.apps.education.fr/iremlorrainegroupejeux/jaiquia](https://forge.apps.education.fr/iremlorrainegroupejeux/jaiquia)